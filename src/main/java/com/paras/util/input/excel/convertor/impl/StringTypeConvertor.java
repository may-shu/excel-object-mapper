package com.paras.util.input.excel.convertor.impl;

import com.paras.util.input.excel.convertor.TypeConvertor;

/**
 * Implementation of TypeConvertor.
 * It takes responsibility of translating a value read from excel into a String value.
 * 
 * @author Paras.
 *
 */
public class StringTypeConvertor implements TypeConvertor<String>{

	public String convert( Object value, String ... pattern ) {
		/*
		 * If value is instance of String.
		 * Simple, cast it and return. 
		 */
		if( value instanceof String ) {
			return ( String ) value;
		}
		
		/*
		 * Otherwise, simply return null.
		 */
		return null;
	}
	
}
