package com.paras.util.input.excel.convertor.impl;

import java.math.BigDecimal;

import com.paras.util.input.excel.convertor.TypeConvertor;

/**
 * Implementation of TypeConvertor.
 * It takes responsibility of converting value into boolean.
 * @author mayan
 *
 */
public class BooleanTypeConvertor implements TypeConvertor<Boolean>{

	@Override
	public Boolean convert( Object value, String ... pattern ) {
		
		/*
		 * If value is false, we can simply return false.
		 */
		if( null == value ) {
			return Boolean.FALSE;
		}
		
		/*
		 * If value is an instance of boolean. 
		 */
		if( value instanceof Boolean ) {
			return (Boolean) value;
		}
		
		/*
		 * If value is instance of String.
		 */
		if( value instanceof String ) {
			if( "true".equalsIgnoreCase( (String) value )) {
				return Boolean.TRUE;
			}
			
			return Boolean.FALSE;
		}
		
		/*
		 * If value is an instance of BigInteger.
		 */
		if( value instanceof BigDecimal ) {
			BigDecimal num = ( BigDecimal ) value;
			
			int val = num.intValue();
			
			if( val == 1 ) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		}
		
		return Boolean.FALSE;
	}
	
}
