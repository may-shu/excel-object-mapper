package com.paras.util.input.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.paras.util.input.helper.ForEach;
import com.paras.util.input.helper.ReflectionHelper;

/**
 * Mapper class that will actually map excel rows to object.
 * @author Paras.
 *
 */
public class ObjectMapper {

	private static Logger LOGGER = Logger.getLogger( ObjectMapper.class );
	
	/**
	 * Excel file that will be used for extraction.
	 */
	private final InputStream stream;
	
	/**
	 * Class definition that will be used for extraction.
	 */
	private Class<?> clazz;
	
	/**
	 * Private constructor, so that we can restrict initialization.
	 * @param excel : Excel File to extract from.
	 */
	private ObjectMapper( InputStream stream ) {
		this.stream = stream;
	}
	
	/**
	 * Static Constructor to ensure that file reference is stored properly.
	 * @param excel File object referring to excel file.
	 * @return
	 * @throws FileNotFoundException 
	 */
	public static ObjectMapper mapFrom( File excel ) throws FileNotFoundException {
		InputStream stream = new FileInputStream( excel );
		return new ObjectMapper( stream );
	}
	
	/**
	 * Static Constructor to ensure that we have an excel to read.
	 */
	public static ObjectMapper mapFrom( InputStream in ) {
		return new ObjectMapper( in );
	}
	
	/**
	 * Name of the class. A row in excel will be mapped to instance of this class.
	 * @param clazz Name of class.
	 * @return
	 */
	public ObjectMapper to( Class<?> clazz ) {
		this.clazz = clazz;
		return this;
	}
	
	/**
	 * Maps Excel Sheet Rows to List of Passed Class Definition.
	 * @return List of instance of class.
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> list() throws Throwable {		
		LOGGER.info( "In ObjectMapper | Starting Execution of list" );

		List<T> items = new ArrayList<T>();
		
		try{
			Iterator<Row> rowIterator;
			
			Workbook workbook = getWorkbook( this.stream );
			int sheets = workbook.getNumberOfSheets();
			
			/**
			 * Let's Go through each of the sheets.
			 */
			for( int index = 0; index<sheets; index++ ) {
				
				/**
				 * Let's retrieve the sheet at index.
				 */
				Sheet sheet = workbook.getSheetAt( index );
				rowIterator = sheet.iterator();
				
				Map<String, Integer> map = new HashMap<String, Integer>();
				
				while( rowIterator.hasNext() ) {
					Row row = rowIterator.next();
					
					int rowNum = row.getRowNum();
					
					if( rowNum == 0 ) {
						readHeader( row, map );
					} else {
						items.add( (T) readExcelBody(row, map));
					}
				}
			}
		}
		catch( FileNotFoundException ex ) {
			LOGGER.error( "In ObjectMapper | In list | Caught FileNotFoundException | " + ex.getMessage());
		}
		finally{
			if( stream!= null ) {
				stream.close();
			}
		}
		
		LOGGER.info( "In ObjectMapper | Finished Execution of list" );
		
		return items;
	}
	
	/**
	 * Create A WorkBook instance from input stream.
	 * Since, we can either receive a xls file or xlsx file.
	 * We need to differentiate the two, so that we always pick the right implementations to operate on them.
	 * @throws InvalidFormatException 
	 */
	private Workbook getWorkbook( InputStream in ) throws IOException, InvalidFormatException {
		Workbook book = WorkbookFactory.create( in );
		return book;
	}
	
	/**
	 * Read Excel Header Row.
	 */
	private void readHeader( final Row row, final Map<String, Integer> map ) {
		ReflectionHelper.forEachField( this.clazz, new ForEach() {
			
			@Override
			public void field(Field field, String name) {
				
				MapperUtil.createClassFieldToColumnIndexMap(row, name, map);
				
			}
		});
	}
	
	/**
	 * Read Excel Content.
	 */
	private Object readExcelBody( final Row row, final Map<String, Integer> map ) {
		try{
			final Object instance = this.clazz.newInstance();
			
			ReflectionHelper.forEachField(this.clazz, new ForEach() {
				
				@Override
				public void field(Field field, String name) {
					ReflectionHelper.setValueOnField(instance, field, MapperUtil.readColumnValue(row, name, map));				
				}
			});
			
			return instance;
		}
		catch( IllegalAccessException ex ) {
			LOGGER.error( "In ObjectMapper | In readExcelBody | Caught IllegalAccessException | " + ex.getMessage());
		}
		catch ( InstantiationException ex ) {
			LOGGER.error( "In ObjectMapper | In readExcelBody | Caught InstantiationException | " + ex.getMessage());
		}
		
		return null;
	}
	
	/**
	 * Read A Sheet.
	 */
	@SuppressWarnings("unchecked")
	private <T> List<T> list( Sheet sheet ){		
		LOGGER.info( "In ObjectMapper | Starting Execution of map of a list " );

		List<T> items = new ArrayList<T>();	
		Iterator<Row> rowIterator;
		
		rowIterator = sheet.iterator();		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		while( rowIterator.hasNext() ) {
			Row row = rowIterator.next();
			
			int rowNum = row.getRowNum();
			
			if( rowNum == 0 ) {
				readHeader( row, map );
			} else {
				items.add( (T) readExcelBody(row, map));
			}
		}
		
		LOGGER.info( "In ObjectMapper | Finished Execution of map of a list " );
		
		return items;
	}
	
	/**
	 * Maps Excel Sheet Rows to List of Passed Class Definition.
	 * @param <T>
	 * @return List of instance of class.
	 * @throws Throwable
	 */
	public <T> Map<String, List<T>> map() throws Throwable {		
		LOGGER.info( "In ObjectMapper | Starting Execution of list" );

		Map<String, List<T>> sheetMap = new HashMap<String, List<T>>();
		List<T> items = new ArrayList<T>();
		
		try{
			
			Workbook workbook = getWorkbook( this.stream );
			int sheets = workbook.getNumberOfSheets();
			
			/**
			 * Let's Go through each of the sheets.
			 */
			for( int index = 0; index<sheets; index++ ) {
				
				/**
				 * Let's retrieve the sheet at index.
				 */
				Sheet sheet = workbook.getSheetAt( index );
				String sheetName = sheet.getSheetName();
				
				items = list( sheet );
				sheetMap.put( sheetName, items );
			}
		}
		catch( FileNotFoundException ex ) {
			LOGGER.error( "In ObjectMapper | In list | Caught FileNotFoundException | " + ex.getMessage());
		}
		finally{
			if( stream!= null ) {
				stream.close();
			}
		}
		
		LOGGER.info( "In ObjectMapper | Finished Execution of list" );
		
		return sheetMap;
	}
}
