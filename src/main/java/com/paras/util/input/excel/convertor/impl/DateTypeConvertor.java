package com.paras.util.input.excel.convertor.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.paras.util.input.excel.convertor.TypeConvertor;

/**
 * Implementation of TypeConvertor.
 * It takes responsibility of translating a value read from excel into a date.
 * @author Paras.
 *
 */
public class DateTypeConvertor implements TypeConvertor<Date>{

	@Override
	public Date convert( Object value, String ... pattern ) {
		
		/*
		 * If value is null. 
		 * Return null. Simple. ! 
		 */		
		if( null == value ) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance();
		
		if( value instanceof Timestamp ) {
			calendar.setTimeInMillis( (( Timestamp ) value).getTime() );
		} else if( value instanceof Date ) {
			calendar.setTimeInMillis( ((Date) value ).getTime());
		} else if( value instanceof String ) {
			
			String requiredPattern = pattern[0];
			try{
				return new SimpleDateFormat( requiredPattern ).parse( (String) value );
			} 
			catch( Exception ex ) {
				return null;
			}
		} else {
			return null;
		}
		
		return calendar.getTime();
	}
	
}
