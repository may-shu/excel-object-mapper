package com.paras.util.input.excel.convertor;

/**
 * Util class to translate a field into appropriate datatype.
 * @author Paras.
 *
 */
public interface TypeConvertor<T> {

	/**
	 * Convert value into an instance of type T.
	 * @param value
	 * @param pattern
	 * @return
	 */
	T convert(Object value, String ... pattern );
	
}
