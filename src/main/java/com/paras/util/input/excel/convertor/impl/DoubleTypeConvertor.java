package com.paras.util.input.excel.convertor.impl;

import java.math.BigDecimal;

import com.paras.util.input.excel.convertor.TypeConvertor;

/**
 * Implementation of TypeConvertor.
 * It takes responsibility of translating a value read from excel into a double value.
 * 
 * @author Paras.
 *
 */
public class DoubleTypeConvertor implements TypeConvertor<Double>{

	@Override
	public Double convert( Object value, String ... pattern ) {
		
		/*
		 * If value is null. 
		 * Return null. Simple. ! 
		 */
		if( null == value ) {
			return null;
		}
		
		/*
		 * If value is already an instance of Double.
		 * No special treatment required then. 
		 */
		if( value instanceof Double ) {
			return (Double) value;
		}
		
		/*
		 * If value is instance of string.
		 */
		if( value instanceof String ) {
			try{
				return Double.valueOf((String) value);
			} catch ( Exception ex ) {
				
			}
		}
		
		/*
		 * If value instance of BigDecimal.
		 */
		if( value instanceof BigDecimal ) {
			return ((BigDecimal) value ).doubleValue();
		}
		
		return null;
	}
}
