package com.paras.util.input.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;

/**
 * An annotation that will describe field to column mapping.
 * It can later be used in translating excel row to model field.
 * @author Paras.
 *
 */
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Column {
	/**
	 * Name of column to which this field will be mapped.
	 */
	String name() default "";
	
	/**
	 * Format to be used in case of date.
	 */
	String format() default "";
}
