package com.paras.util.input.excel;

import java.io.File;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * Utility class to perform common tasks on behalf of object mapper.
 * @author Paras.
 *
 */
class MapperUtil {

	/**
	 * If we are dealing with a file with older version ?
	 */
	public static boolean isFileInOldExcelFormat( File file ) {
		return file.getName().endsWith( ".xls" );
	}
	
	/**
	 * Find a Cell's Index In row by Its header Value.
	 */
	public static int findIndexByHeader( Row row, String name ) {
		Iterator<Cell> cellIterator = row.cellIterator();
		
		while( cellIterator.hasNext() ) {
			Cell cell = cellIterator.next();
			String value = getCellValue( cell ).trim();
			
			if( name.equalsIgnoreCase( value )) {
				return cell.getColumnIndex();
			}
		}
		
		return -1;
	}
	
	/**
	 * Read a cell and extract its value.
	 */
    private static String getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }

        String value = "";
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                value += String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                value += new BigDecimal(cell.getNumericCellValue()).toString();
                break;
            case Cell.CELL_TYPE_STRING:
                value += cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
            	switch( cell.getCachedFormulaResultType()) {
	            	case Cell.CELL_TYPE_NUMERIC :
	            		value += cell.getNumericCellValue();
	            		break;
	            	
	            	case Cell.CELL_TYPE_STRING :
	            		value += cell.getRichStringCellValue();
	            		break;
            	}
        }

        return value;
    }
    
	/**
	 * Create a map of header names to index.
	 */
	public static void createClassFieldToColumnIndexMap( Row row, String name, Map<String, Integer> map) {
		int index = MapperUtil.findIndexByHeader(row, name);
		
		if( index != -1 ) {
			map.put( name, index );
		}
	}
	
	/**
	 * Read a column's value from its name.
	 * Column to index mapping is stored in map.
	 */
	public static String readColumnValue( final Row row, String name, Map<String, Integer> map ) {
		Integer index = map.get( name );
		
		if( index == null ) {
			return null;
		}
		Cell cell = row.getCell( index );
		return getCellValue( cell );
	}
}
