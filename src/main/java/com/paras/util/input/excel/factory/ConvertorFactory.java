package com.paras.util.input.excel.factory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.paras.util.input.excel.convertor.TypeConvertor;
import com.paras.util.input.excel.convertor.impl.BooleanTypeConvertor;
import com.paras.util.input.excel.convertor.impl.DateTypeConvertor;
import com.paras.util.input.excel.convertor.impl.DoubleTypeConvertor;
import com.paras.util.input.excel.convertor.impl.IntegerTypeConvertor;
import com.paras.util.input.excel.convertor.impl.StringTypeConvertor;

/**
 * Factory class to have a map.
 * @author Paras.
 *
 */
public class ConvertorFactory {

	@SuppressWarnings("rawtypes")
	/**
	 * Map to store convertors so that they can be used whenever needed.
	 */
	private static Map<Class, TypeConvertor> map;
	
	/**
	 * Create a map.
	 */
	@SuppressWarnings("rawtypes")
	private static void init() {
		
		map = new HashMap<Class, TypeConvertor>();
		
		map.put( Boolean.class, new BooleanTypeConvertor());
		map.put( Date.class, new DateTypeConvertor());
		map.put( Double.class, new DoubleTypeConvertor());
		map.put( Integer.class, new IntegerTypeConvertor());
		map.put( String.class, new StringTypeConvertor());
	}
	
	/**
	 * Retrieve the map.
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map<Class, TypeConvertor> get() {
		if( map == null ) {
			init();
		}
		
		return map;
	}
}
