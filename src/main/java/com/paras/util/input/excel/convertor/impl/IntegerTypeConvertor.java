package com.paras.util.input.excel.convertor.impl;

import java.math.BigDecimal;

import com.paras.util.input.excel.convertor.TypeConvertor;

/**
 * Implementation of TypeConvertor.
 * It takes responsibility of translating a value read from excel into a integer value.
 * 
 * @author Paras.
 *
 */
public class IntegerTypeConvertor implements TypeConvertor<Integer>{

	@Override
	public Integer convert( Object value, String ... pattern ) {
		/*
		 * If value is null, simply return null.
		 */
		if( null == value ) {
			return null;
		}
		
		/*
		 * If value is instance of Integer.
		 * Return as it is.
		 */
		if( value instanceof Integer ) {
			return ( Integer ) value;
		}
		
		/*
		 * If value is instance of String. 
		 */
		if( value instanceof String ) {
			try{
				return Integer.parseInt( ( String ) value );
			}
			catch( Exception ex ) {
				
			}
		}
		
		/*
		 * If value is instance of BigDecimal. 
		 */
		if( value instanceof BigDecimal ) {
			return ((BigDecimal) value).intValue();
		}
		
		return null;
	}
	
}
