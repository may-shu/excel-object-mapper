package com.paras.util.input.helper;

import java.lang.reflect.Field;

/**
 * An interface open to implementation.
 * This interface dictates that an operation or method would be executed for each of the field of a class.
 * @author Paras.
 *
 */
public interface ForEach {

	/**
	 * Method to be executed for each field.
	 * @param field
	 * @param name
	 */
	public void field( Field field, String name );
}
