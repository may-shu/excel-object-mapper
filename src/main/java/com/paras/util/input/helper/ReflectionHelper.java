package com.paras.util.input.helper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;

import com.paras.util.input.excel.annotation.Column;
import com.paras.util.input.excel.convertor.TypeConvertor;
import com.paras.util.input.excel.factory.ConvertorFactory;

/**
 * Utility class to help with reflection.
 * @author Paras.
 *
 */
public class ReflectionHelper {

	private static Logger LOGGER = Logger.getLogger( ReflectionHelper.class );
	
	/**
	 * Make first letter capital.
	 */
	private static String toFirstLetterCapital( String str ) {
		return str.substring(0, 1).toUpperCase() + str.substring( 1 );
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	/**
	 * This method will set value on the field.
	 * First it will convert to appropriate type and then convert according to the configuration provided.
	 * @param instance
	 * @param field
	 * @param value
	 */
	public static void setValueOnField( Object instance, Field field, Object value ) {
		/*
		 * Retrieve class for the instance.
		 */
		Class clazz = instance.getClass();
		String methodName = "set" + toFirstLetterCapital( field.getName() );
		
		Map<Class, TypeConvertor> map = ConvertorFactory.get();
		
		
		for( Map.Entry<Class, TypeConvertor> entry : map.entrySet()) {
			if( field.getType().equals(entry.getKey())) {
				
				Method method;
				try {
					method = clazz.getDeclaredMethod( methodName, entry.getKey());
					Column column = field.getAnnotation( Column.class );
					method.invoke( instance, entry.getValue().convert( value, column == null ? null : column.format()));
				} catch (NoSuchMethodException ex) {
					LOGGER.error( "In ReflectionUtil | Caught NoSuchMethodException | " + ex.getMessage());
					ex.printStackTrace();
				} catch (SecurityException ex) {
					LOGGER.error( "In ReflectionUtil | Caught SecurityException | " + ex.getMessage());
					ex.printStackTrace();
				} catch (IllegalAccessException ex) {
					LOGGER.error( "In ReflectionUtil | Caught IllegalAccessException | " + ex.getMessage());
					ex.printStackTrace();
				} catch (IllegalArgumentException ex) {
					LOGGER.error( "In ReflectionUtil | Caught IllegalArgumentException | " + ex.getMessage());
					ex.printStackTrace();
				} catch (InvocationTargetException ex) {
					LOGGER.error( "In ReflectionUtil | Caught InvocationTargetException | " + ex.getMessage());
					ex.printStackTrace();
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static void forEachField( Class clazz, ForEach forEach ) {
		Field[] fields = clazz.getDeclaredFields();
		
		int size = fields.length;
		
		if( size > 0 ) {
			for( Field field : fields ) {
				forEach.field( field, field.isAnnotationPresent( Column.class ) ? field.getAnnotation( Column.class ).name() : field.getName());;
			}
		}
	}
	
}
